import settings
import vk

session = vk.Session()
api = vk.API(session, v=5.0)

def send_message(message='test', token=settings.token):
    if settings.vk_ids != '' and token != '':
        for x in range(len(settings.vk_ids)):
            id = settings.vk_ids[x]
            api.messages.send(message=message, access_token=token, user_id=id)

def check_message():
    messages = api.messages.getConversations(count=10, access_token=settings.token)
    last = messages['items'][0]['last_message']['text']
    if '/user_name=' in last and 'You can change parameters' not in last:
        settings.user_name = last.split('=')[-1]
        send_message(message='Gitlab user_name теперь - '+settings.user_name)
    if '/token=' in last and 'You can change parameters' not in last:
        new_token = last.split('=')[-1]
        if new_token.isdigit():
            settings.token = new_token
            send_message(message='Gitlab token теперь - '+settings.token)
        else:
            send_message(message='Некорректный gitlab token!')
    if '/help' == last:
        send_message(message='You can change parameters: \n user_name - ваш имя пользователя в Gitlab, формат /user_name=ваше_имя \n token - ваш токен доступа к Gitlab, формат /token=ваш токен')
    return 'ok'
