#!/usr/bin/python
import os
import requests
import sys
import logging

from vkapi import *
from settings import *
from flask import Flask
from flask import request

app = Flask(__name__)
app.logger.addHandler(logging.StreamHandler(sys.stdout))
app.logger.setLevel(logging.DEBUG)
PORT = int(os.environ.get('PORT', 5000))

def main():
    send_message(message='Send /help for information')


@app.route('/', methods=['GET','POST'])
def webhook():
    if request.method == "POST":
        if request.is_json:
            data = request.get_json()
            app.logger.info(data)
            if 'object_kind' in data:
                kind = data['object_kind']
                if kind == 'push' and data['ref'] == 'refs/heads/master':
                    app.logger.info('push_hook')
                    push_hook(data)
                elif kind == 'merge_request':
                    if 'action' in str(data):
                        if data['object_attributes']['action'] == 'open' or data['object_attributes']['action'] == 'reopen':
                            app.logger.info('mergehook_you_aproover')
                            mergehook_you_aproover(data)
                        if data['object_attributes']['action'] == 'approved':
                            app.logger.info('mergehook_is_approved')
                            app.logger.info(settings.user_name)
                            mergehook_is_approved(data)
                elif kind == 'note' and data['object_attributes']['noteable_type'] == 'MergeRequest':
                    app.logger.info('merge_note_hook')
                    merge_note_hook(data)
                elif kind == 'tag_push' and data['before'] == '0000000000000000000000000000000000000000':
                    app.logger.info('tag_hook')
                    tag_hook(data)
                else:
                    return 'Wrong JSON', 403
            else:
                return 'No JSON', 403
        else:
            return 'Not Found', 404
    return ''

def get_approvers_by_project_id(data):
    app.logger.info('get_approvers_by_project_id')
    is_approover =False
    project_id = data['project']['id']
    if data['object_kind'] == 'merge_request':
        request_iid = data['object_attributes']['iid']
    else:
        request_iid = data['merge_request']['iid']
    headers = {'Content-Type': 'application/json'}
    approvers_list =requests.get('https://gitlab.com/api/v4/projects/'+str(project_id)+'/merge_requests/'+str(request_iid)+'/approvals?private_token='+str(gitlab_token), headers).json()
    for x in approvers_list['approver_groups']:
        app_l = requests.get('https://gitlab.com/api/v4//groups/'+str(x['group']['id'])+'/members?private_token='+str(gitlab_token), headers).json()
        if user_name in str(app_l):
            is_approover = True
            break
    if user_name in str(approvers_list['approvers']):
        is_approover = True
    return is_approover

# def get_user_by_id(data):
#     app.logger.info('get_user_by_id')
#     id = data['object_attributes']['author_id']
#     url = 'https://gitlab.com/api/v4/users/'+str(id)+'?private_token='+str(gitlab_token)
#     headers = {'Content-Type': 'application/json'}
#     user = requests.get(url, headers).json()
#     app.logger.info(user)
#     return user

def get_merge_author(data):
    project_id = data['project']['id']
    request_iid = data['object_attributes']['iid']
    headers = {'Content-Type': 'application/json'}
    merge = requests.get('https://gitlab.com/api/v4/projects/'+ str(project_id) + '/merge_requests/'+str(request_iid) +'?private_token='+str(gitlab_token), headers).json()
    app.logger.info(str(merge))
    merge_author = merge['author']['username']
    app.logger.info('merge_author')
    return merge_author

def get_project_tags(data):
    app.logger.info('get_project_tags')
    project_id = data['project']['id']
    url = 'https://gitlab.com/api/v4/projects/'+str(project_id)+'/repository/tags?order_by=updated&private_token='+str(gitlab_token)
    headers = {'Content-Type': 'application/json'}
    tags = requests.get(url, headers).json()
    app.logger.info(tags)
    return tags

def push_hook(data):
    commit_url = data['commits'][0]['url']
    send_message('Новый коммит в master. \n Ссылка на коммит: '+commit_url)

def mergehook_you_aproover(data):
    request_url = data['object_attributes']['url']
    is_approover = get_approvers_by_project_id(data)
    if is_approover == True:
        send_message('Новый мержреквест, вы - апрувер. \n Ссылка на мержреквест: ' + request_url)

def mergehook_is_approved(data):
    app.logger.info(settings.user_name)
    request_url = data['object_attributes']['url']
    user = get_merge_author(data)
    app.logger.info(user)
    app.logger.info(settings.user_name)
    if str(user) == str(settings.user_name):
        send_message('Ваш мерджреквест заапрувлен. \n Ссылка на мержреквест: ' + request_url)

def tag_hook(data):
    tag = data['ref']
    if 'refs/tags/release_' in tag:
        ver = tag.split('/')[-1]
        tags = get_project_tags(data)
        link = data['project']['web_url']
        current_tag = tags[0]['name']
        previous_tag = tags[1]['name']
        send_message('Новый тэг: '+str(ver)+' \n Ссылка на diff с предыдущим тегом:'+str(link)+'/compare/'+str(previous_tag)+'...'+str(current_tag))

def merge_note_hook(data):
    app.logger.info(data)
    request_url = data['object_attributes']['url']
    request_url = request_url.split('#')[0]
    is_approover = get_approvers_by_project_id(data)
    if is_approover == True:
        send_message('Новый комментарий к мержреквесту, где вы - аппрувер. \n Ссылка на мержреквест: ' + request_url)

@app.route('/vk', methods=['POST'])
def vk_receiver():
    data = request.get_json()
    if 'type' not in data:
        return 'Not VK', 403
    if data['type'] == 'confirmation':
        return confirmation_token
    elif data['type'] == 'message_new':
        app.logger.info('new_message')
        app.logger.info(str(data))
        if data['object']['user_id'] not in settings.vk_ids:
            settings.vk_ids.append(data['object']['user_id'])
        app.logger.info(settings.vk_ids)
        check_message()
        return 'OK'
    return 'OK'

if __name__ == '__main__':
    print("go")
    app.run(host='0.0.0.0', port = PORT, debug=True)
